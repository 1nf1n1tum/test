#property copyright "Copyright @2007, Yohanes R. Gagahlin. http://www.fxhope.com"
#property link      "http://www.fxhope.com"
#property show_inputs

int Gi_76 = D'01.07.2009 03:00';
int Gi_80 = 111111;
extern int Risk_Setting = 10;
extern int Custom_ProfitTarget = 10000;
extern bool Always_ActivateCustomTarget = FALSE;
extern bool Aggressive_Trading = FALSE;
extern double Multiply = 0.1;
int G_slippage_108 = 3;
double Gd_112 = 1000.0;
double G_pips_120 = 50.0;
double G_pips_128 = 100.0;
int Gi_136 = 30;
int G_period_140 = 50;
double G_global_var_144;
int G_magic_152 = 12345;
string G_var_name_156 = "Last Watermark";
string G_var_name_164 = "Lot Trading";
string G_var_name_172 = "Fractal Up";
string G_var_name_180 = "Fractal Down";
string G_var_name_188 = "TFractal Down";
string G_var_name_196 = "TFractal Up";
string G_var_name_204 = "New Signal Buy";
string G_var_name_212 = "New Signal Sell";
string Gs_pos_220 = "Pos";
double Gd_228;
double G_global_var_236;
double G_global_var_244;
double G_global_var_252;
double G_global_var_260;
int G_global_var_268 = 0;
int G_global_var_272 = 0;
int G_global_var_276 = 2;
int Gi_unused_280 = 0;
double Gda_unused_292[100];
double Gd_304;
double Gd_312;
double Gd_320;

int CekTimeLimit() {
   if (TimeCurrent() > Gi_76) {
      Alert("You need to update this program!\nContact the provider at http://www.FxHope.com");
      return (0);
   }
   int Li_0 = TimeDayOfYear(Gi_76) - TimeDayOfYear(TimeCurrent());
   if (Li_0 < 0) Li_0 += 365;
   return (1);
}

int CekCondition() {
   int acc_number_4;
   bool bool_0 = IsDemo();
   if (!bool_0) {
      acc_number_4 = AccountNumber();
      if (acc_number_4 != Gi_80) {
         Alert("You can not use account no (" + DoubleToStr(acc_number_4, 0) + ") with this program!");
         return (0);
      }
   }
   if (!CekTimeLimit()) return (0);
   bool bool_8 = IsTesting();
   if (bool_8) {
      Alert("You can not back testing. Go to fxhope.com for back testing results and videos");
      return (0);
   }
   if (Symbol() != "EURUSD" && Symbol() != "EURUSDm") {
      Alert("This EA version run on EURUSD");
      return (0);
   }
   if (Period() != PERIOD_H4) {
      Alert("Please change to H4 Period.");
      return (0);
   }
   return (1);
}

int init() {
   int order_total_0;
   HideTestIndicators(TRUE);
   Gd_228 = GlobalVariableGet(G_var_name_164);
   if (GetLastError() != 0/* NO_ERROR */) {
      order_total_0 = OrdersTotal();
      if (order_total_0 > 0) {
         OrderSelect(1, SELECT_BY_POS, MODE_TRADES);
         Gd_228 = OrderLots();
         GlobalVariableSet(G_var_name_164, Gd_228);
      } else {
         Gd_228 = 0.01;
         GlobalVariableSet(G_var_name_164, Gd_228);
      }
   }
   G_global_var_144 = GlobalVariableGet(G_var_name_156);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_144 = AccountEquity();
      GlobalVariableSet(G_var_name_156, G_global_var_144);
   }
   G_global_var_236 = GlobalVariableGet(G_var_name_172);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_236 = 0;
      GlobalVariableSet(G_var_name_172, G_global_var_236);
   }
   G_global_var_244 = GlobalVariableGet(G_var_name_180);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_244 = 0;
      GlobalVariableSet(G_var_name_180, G_global_var_244);
   }
   G_global_var_260 = GlobalVariableGet(G_var_name_196);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_260 = 0;
      GlobalVariableSet(G_var_name_196, G_global_var_260);
   }
   G_global_var_252 = GlobalVariableGet(G_var_name_188);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_252 = 0;
      GlobalVariableSet(G_var_name_188, G_global_var_252);
   }
   G_global_var_268 = GlobalVariableGet(G_var_name_204);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_268 = 0;
      GlobalVariableSet(G_var_name_204, G_global_var_268);
   }
   G_global_var_272 = GlobalVariableGet(G_var_name_212);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_272 = 0;
      GlobalVariableSet(G_var_name_212, G_global_var_272);
   }
   G_global_var_276 = GlobalVariableGet(Gs_pos_220);
   if (GetLastError() != 0/* NO_ERROR */) {
      G_global_var_276 = 2;
      GlobalVariableSet(Gs_pos_220, G_global_var_276);
   }
   return (0);
}

int deinit() {
   return (0);
}

double CloseProfit() {
   int cmd_12;
   bool is_closed_16;
   int order_total_8 = OrdersTotal();
   for (int pos_0 = order_total_8 - 1; pos_0 >= 0; pos_0--) {
      OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES);
      cmd_12 = OrderType();
      is_closed_16 = FALSE;
      switch (cmd_12) {
      case OP_BUY:
         if (OrderMagicNumber() != G_magic_152) break;
         is_closed_16 = OrderClose(OrderTicket(), OrderLots(), MarketInfo(OrderSymbol(), MODE_BID), G_slippage_108, LightBlue);
         break;
      case OP_SELL:
         if (OrderMagicNumber() != G_magic_152) break;
         is_closed_16 = OrderClose(OrderTicket(), OrderLots(), MarketInfo(OrderSymbol(), MODE_ASK), G_slippage_108, LightBlue);
      }
   }
   if (AccountEquity() > G_global_var_144) {
      G_global_var_144 = AccountEquity();
      GlobalVariableSet(G_var_name_156, G_global_var_144);
   }
   return (0.0);
}

int MyOrdersTotal(int Ai_0) {
   int cmd_16;
   int order_total_8 = OrdersTotal();
   int count_12 = 0;
   if (Ai_0 == 0) {
      order_total_8 = OrdersTotal();
      if (order_total_8 <= 0) return (count_12);
      for (int pos_4 = order_total_8 - 1; pos_4 >= 0; pos_4--) {
         OrderSelect(pos_4, SELECT_BY_POS, MODE_TRADES);
         cmd_16 = OrderType();
         if (cmd_16 == OP_BUY || cmd_16 == OP_SELL && OrderMagicNumber() == G_magic_152) count_12++;
      }
      return (count_12);
   }
   if (Ai_0 == 1) {
      order_total_8 = OrdersTotal();
      if (order_total_8 <= 0) return (count_12);
      for (pos_4 = order_total_8 - 1; pos_4 >= 0; pos_4--) {
         OrderSelect(pos_4, SELECT_BY_POS, MODE_TRADES);
         cmd_16 = OrderType();
         if (cmd_16 == OP_BUY && OrderMagicNumber() == G_magic_152) count_12++;
      }
      return (count_12);
   }
   if (Ai_0 == 2) {
      order_total_8 = OrdersTotal();
      if (order_total_8 <= 0) return (count_12);
      for (pos_4 = order_total_8 - 1; pos_4 >= 0; pos_4--) {
         OrderSelect(pos_4, SELECT_BY_POS, MODE_TRADES);
         cmd_16 = OrderType();
         if (cmd_16 == OP_SELL && OrderMagicNumber() == G_magic_152) count_12++;
      }
      return (count_12);
   }
   return (0);
}

double RemoveOrder() {
   int cmd_12;
   bool is_closed_16;
   int order_total_8 = OrdersTotal();
   for (int pos_0 = order_total_8 - 1; pos_0 >= 0; pos_0--) {
      OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES);
      cmd_12 = OrderType();
      is_closed_16 = FALSE;
      switch (cmd_12) {
      case OP_BUYSTOP:
      case OP_BUYLIMIT:
         if (OrderMagicNumber() != G_magic_152) break;
         is_closed_16 = OrderClose(OrderTicket(), OrderLots(), OrderOpenPrice(), G_slippage_108, LightBlue);
         break;
      case OP_SELLSTOP:
      case OP_SELLLIMIT:
         if (OrderMagicNumber() != G_magic_152) break;
         is_closed_16 = OrderClose(OrderTicket(), OrderLots(), OrderOpenPrice(), G_slippage_108, LightBlue);
      }
   }
   return (0.0);
}

int CalculateLotsTarget() {
   int Li_0;
   if (Risk_Setting <= 0) Risk_Setting = 1;
   else
      if (Risk_Setting > 20) Risk_Setting = 20;
   if (Symbol() == "EURUSD") {
      Li_0 = 25000;
      Gd_228 = G_global_var_144 / Li_0 * (Risk_Setting / 10.0);
      GlobalVariableSet(G_var_name_164, Gd_228);
   } else {
      if (Symbol() == "EURUSDm") {
         Li_0 = 2500;
         Gd_228 = G_global_var_144 / Li_0 * (Risk_Setting / 10.0);
         GlobalVariableSet(G_var_name_164, Gd_228);
      }
   }
   if (Gd_228 < 0.01) {
      Gd_228 = 0.01;
      GlobalVariableSet(G_var_name_164, Gd_228);
   }
   if (Aggressive_Trading) {
      if (Multiply <= 0.0) Multiply = 0;
      if (Multiply > 0.5) Multiply = 0.5;
      if (AccountEquity() < 0.7 * G_global_var_144) {
         Gd_228 *= (5.0 * Multiply + 1.0);
         GlobalVariableSet(G_var_name_164, Gd_228);
      } else {
         if (AccountEquity() < 0.8 * G_global_var_144) {
            Gd_228 *= (3.0 * Multiply + 1.0);
            GlobalVariableSet(G_var_name_164, Gd_228);
         } else {
            if (AccountEquity() < 0.9 * G_global_var_144) {
               Gd_228 *= (1.0 * Multiply + 1.0);
               GlobalVariableSet(G_var_name_164, Gd_228);
            }
         }
      }
   }
   if (Symbol() == "EURUSD") Gd_304 = Gd_112 * Gd_228;
   else
      if (Symbol() == "EURUSDm") Gd_304 = Gd_112 * Gd_228 / 10.0;
   return (1);
}

int start() {
   string Ls_36;
   string Ls_44;
   string Ls_52;
   string Ls_60;
   string Ls_68;
   string Ls_76;
   string Ls_84;
   string Ls_92;
   int ticket_100;
   if (!CekCondition()) return (0);
   int Li_32 = 2;
   double ima_0 = iMA(NULL, 0, G_period_140, 0, MODE_SMA, PRICE_CLOSE, 0);
   double Ld_8 = Li_32 * iATR(NULL, 0, G_period_140, 0);
   double Ld_16 = ima_0 + Ld_8;
   double Ld_24 = ima_0 - Ld_8;
   if (MyOrdersTotal(0) == 0) {
      G_global_var_276 = 2;
      GlobalVariableSet(Gs_pos_220, G_global_var_276);
      if (iFractals(NULL, 0, MODE_LOWER, 3) != 0.0) {
         G_global_var_244 = iFractals(NULL, 0, MODE_LOWER, 3);
         GlobalVariableSet(G_var_name_180, G_global_var_244);
         if (G_global_var_252 != G_global_var_244) {
            G_global_var_268 = 1;
            GlobalVariableSet(G_var_name_204, G_global_var_268);
            G_global_var_252 = G_global_var_244;
            GlobalVariableSet(G_var_name_180, G_global_var_244);
         }
      }
      if (iFractals(NULL, 0, MODE_UPPER, 3) != 0.0) {
         G_global_var_236 = iFractals(NULL, 0, MODE_UPPER, 3);
         GlobalVariableSet(G_var_name_172, G_global_var_236);
         if (G_global_var_260 != G_global_var_236) {
            G_global_var_272 = 1;
            GlobalVariableSet(G_var_name_212, G_global_var_272);
            G_global_var_260 = G_global_var_236;
            GlobalVariableSet(G_var_name_196, G_global_var_260);
         }
      }
   } else {
      if (Symbol() == "EURUSD") Gd_304 = Gd_112 * Gd_228;
      if (Symbol() == "EURUSDm") Gd_304 = Gd_112 * Gd_228 / 10.0;
      if (AccountEquity() > G_global_var_144 + Gd_304 || (AccountProfit() > Gd_304 && AccountEquity() > G_global_var_144)) {
         CloseProfit();
         RemoveOrder();
      }
      if (Always_ActivateCustomTarget) {
         if (AccountProfit() > Custom_ProfitTarget) {
            CloseProfit();
            RemoveOrder();
         }
      } else {
         if (AccountEquity() > G_global_var_144 + Custom_ProfitTarget || (AccountProfit() > Custom_ProfitTarget && AccountEquity() > G_global_var_144)) {
            CloseProfit();
            RemoveOrder();
         }
      }
   }
   if (G_global_var_144 - AccountEquity() > Gd_312) Gd_312 = G_global_var_144 - AccountEquity();
   if (MyOrdersTotal(0) > Gd_320) Gd_320 = MyOrdersTotal(0);
   if (MyOrdersTotal(0) > 0) {
      Ls_36 = "-";
      Ls_44 = "-";
      Ls_52 = "-";
      Ls_60 = "-";
      Ls_68 = "-";
      Ls_76 = "-";
      Ls_84 = "-";
      Ls_92 = "-";
   }
   if (MyOrdersTotal(0) == 0) {
      if (Bid > G_global_var_236) Ls_36 = "TRUE";
      else Ls_36 = "WAITING";
      if (G_global_var_236 != 0.0) Ls_44 = "TRUE";
      else Ls_44 = "WAITING";
      if (G_global_var_272) Ls_52 = "TRUE";
      else Ls_52 = "WAITING";
      if (Bid < Ld_16) Ls_60 = "TRUE";
      else Ls_60 = "WAITING";
      if (Ask < G_global_var_244) Ls_68 = "TRUE";
      else Ls_68 = "WAITING";
      if (G_global_var_244 != 0.0) Ls_76 = "TRUE";
      else Ls_76 = "WAITING";
      if (G_global_var_268) Ls_84 = "TRUE";
      else Ls_84 = "WAITING";
      if (Ask > Ld_24) Ls_92 = "TRUE";
      else Ls_92 = "WAITING";
      if (Bid > G_global_var_236 && G_global_var_236 != 0.0 && G_global_var_272 && Bid < Ld_16) {
         RemoveOrder();
         CalculateLotsTarget();
         ticket_100 = OrderSend(Symbol(), OP_SELL, Gd_228, Bid, G_slippage_108, Ld_16 + G_pips_120 * Point, Bid - G_pips_128 * Point, "FractalWizard v2 by FxHope.com", G_magic_152,
            0, Red);
         G_global_var_272 = 0;
         GlobalVariableSet(G_var_name_212, G_global_var_272);
         G_global_var_276 = 1;
         GlobalVariableSet(Gs_pos_220, G_global_var_276);
         ticket_100 = OrderSend(Symbol(), OP_SELLLIMIT, Gd_228, Bid + Gi_136 * Point, G_slippage_108, Ld_16 + G_pips_120 * Point, Bid - G_pips_128 * Point, "FractalWizard v2 by FxHope.com",
            G_magic_152, 0, Red);
         return (0);
      }
      if (Ask < G_global_var_244 && G_global_var_244 != 0.0 && G_global_var_268 && Ask > Ld_24) {
         RemoveOrder();
         CalculateLotsTarget();
         ticket_100 = OrderSend(Symbol(), OP_BUY, Gd_228, Ask, G_slippage_108, Ld_24 - G_pips_120 * Point, Ask + G_pips_128 * Point, "FractalWizard v2 by FxHope.com", G_magic_152,
            0, Green);
         G_global_var_268 = 0;
         GlobalVariableSet(G_var_name_204, G_global_var_268);
         G_global_var_276 = 0;
         GlobalVariableSet(Gs_pos_220, G_global_var_276);
         ticket_100 = OrderSend(Symbol(), OP_BUYLIMIT, Gd_228, Ask - Gi_136 * Point, G_slippage_108, Ld_24 - G_pips_120 * Point, Ask + G_pips_128 * Point, "FractalWizard v2 by FxHope.com",
            G_magic_152, 0, Green);
         return (0);
      }
   }
   Comment("                      Program Wizard Fractal ver 2.0 (Build Jan 15, 2008)-For EURUSD Standard/Mini Account-PERIOD H4", 
      "\n                      Copyright@2007 Yohanes R. Gagahlin. http://www.fxhope.com", 
      "\n                      Fractals: " + DoubleToStr(G_global_var_236, 4) + " / " + DoubleToStr(G_global_var_244, 4), 
      "\n                      New Signal SELL: " + Ls_36 + "/" + Ls_44 + "/" + Ls_52 + "/" + Ls_60, 
      "\n                      New Signal BUY : " + Ls_68 + "/" + Ls_76 + "/" + Ls_84 + "/" + Ls_92, 
   "\n                      Last Equity Watermark: " + DoubleToStr(G_global_var_144, 2) + " | Account Equity: " + DoubleToStr(AccountEquity(), 2), " | Lot Size : " + DoubleToStr(Gd_228,
      2));
   return (0);
}
